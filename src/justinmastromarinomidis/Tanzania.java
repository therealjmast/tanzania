package justinmastromarinomidis;

import java.io.File;


import org.jfugue.player.Player;
import org.jfugue.pattern.Pattern;

import org.jfugue.midi.MidiTools;



public class Tanzania {
     public static void main(String[] args) {
    Player player = new Player();
   Pattern song = new Pattern();
   Pattern p1 =  new Pattern("F5i E5i F5i G5i A5q A5g G5q G5q e5h A5i A5i G5i A5i Bb6q Bb6q A5q A5q G5h");
   Pattern p2 = new Pattern("F5i F5i F5i  G5i A5Q A5i A5i G5q Bb6q A5h G5h F5h E5i F5i G5q F5h G5q G5q F5q F5i E5i F5i G5q F5h");
  Pattern p3 = new Pattern("F5q E5i D5i C5h");
  Pattern p4 = new Pattern("F5q C5q F5q C5q");
  Pattern p5 = new Pattern("F5q G5i A5i D5h Bb6i A5i G5q F5h D5i E5i F5i G5q F5h");
     
     song.add(p1);
     song.add(p2);
     song.add(p3);
     song.add(p4);
     song.add(p3);
     song.add(p4);
     song.add(p5);
     File tanzania = new File("./Tanzania.midi");
     
     player.play(song);
    
     }
}
